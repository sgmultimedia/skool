# Skøøl

## Installation

Tout d'abord, commencer par cloner le projet git :

`git clone git@gitlab.com:sgmultimedia/skool.git`

Entrer dans le dossier du projet :

`cd skool`

Lancer le conteneur Docker :

`docker-compose up -d`

Installer les dépendances composer :

`docker-compose exec skool composer install -o`

Installer la base de données :

`docker-compose exec skool php bin/console --env=dev doctrine:schema:create`

Insérer des données de test dans la base : 

`docker-compose exec skool php bin/console --env=dev app:db:insert_test_data 100`

Installer les dépendances node :

`docker-compose exec skool yarn install`

Installer les assets avec Webpack / Encore :

`docker-compose exec skool yarn encore dev`

## Utilisation

La documentation de l'API avec une interface Swagger est disponible à cette adresse :

http://localhost:8080/api/doc

L'interface de gestion se trouve à la racine :

http://localhost:8080

## Évolutions possibles

* Passer toutes routes GET en Query
* Mettre en place des tests unitaires
* Mettre en place de l'analyse de code avec des outils tels que PHPStan et PHPCS
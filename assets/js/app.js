/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */


import '../css/app.scss';
import $ from 'jquery';
import 'bootstrap';
import Vue from 'vue';
import moment from 'moment'

window.Vue = Vue;

const axios = require('axios');

var app = new Vue({
    el: '#appli',
    delimiters: ['${', '}'], // Avoid conflicts with Twig syntax
    data: {
        globalAverageGrade: null,
        students: null,
        stopActions: false, // Marker used to block actions on the UI while something is processing
        currentStudent: null,
        currentStudentGrades: null,
        currentStudentAverageGrade: null,
        newGrade: {},
        savingGrade: false,
        filters: {
            first_name: null,
            last_name: null
        }
    },
    methods: {
        refreshGlobalAverageGrade: function () {
            let self = this;

            axios.get('/api/grades/average')
                .then(function (response) {
                    self.globalAverageGrade = Number.parseFloat(response.data).toPrecision(2);
                })
                .catch(function (error) {
                    console.log(error);
                });
        },

        refreshStudentsList: function (page) {
            let self = this;
            let params = self.filters;
            params.page = page;

            self.students = null; // Display the loader
            self.stopActions = true;

            axios.get(
                '/api/students', {
                    params: params
                }
            )
                .then(function (response) {
                    let result = response.data;

                    result._embedded.items.forEach(function (item) {
                        // Additional properties for interfacing only
                        item.processing = false;
                    });

                    self.students = result;
                })
                .catch(function (error) {
                    console.log(error);
                })
                .then(function () {
                    self.stopActions = false;
                });
        },

        deleteStudent: function (student, message) {
            let self = this;

            if (confirm(message)) {
                self.stopActions = true;
                student.processing = true;

                axios.delete('/api/students/' + student.id)
                    .then(function (response) {
                        self.refreshStudentsList();
                    })
                    .catch(function (error) {
                        console.log(error);
                    })
                    .then(function () {
                        self.stopActions = false;
                        student.processing = false;
                    });
            }
        },

        changeCurrentStudent: function (student) {
            this.currentStudent = student;
            this.currentStudentGrades = null;
            this.currentStudentAverageGrade = null;
            this.newGrade = {};

            this.refreshStudentAverageGrade(student);
            this.refreshStudentGrades(student);
        },

        refreshStudentAverageGrade: function (student) {
            let self = this;

            self.currentStudentAverageGrade = null;

            axios.get('/api/students/' + student.id + '/grades/average')
                .then(function (response) {
                    self.currentStudentAverageGrade = response.data;
                })
                .catch(function (error) {
                    console.log(error);
                });
        },

        refreshStudentGrades: function (student) {
            let self = this;

            self.stopActions = true;
            self.currentStudentGrades = null;

            axios.get('/api/students/' + student.id + '/grades')
                .then(function (response) {
                    self.currentStudentGrades = response.data;
                })
                .catch(function (error) {
                    console.log(error);
                })
                .then(function () {
                    self.stopActions = false;
                });
        },

        createNewGrade: function (student) {
            let self = this;

            self.stopActions = true;
            self.savingGrade = true;

            axios.post(
                '/api/students/' + student.id + '/grades',
                self.newGrade
            )
                .then(function (response) {
                    self.refreshStudentAverageGrade(student);
                    self.refreshStudentGrades(student);

                    self.newGrade = {};
                })
                .catch(function (error) {
                    console.log(error);
                })
                .then(function () {
                    self.stopActions = false;
                    self.savingGrade = false;
                });
        },

        moment: function (date) {
            return moment(date);
        }
    },
    created: function () {
        this.refreshGlobalAverageGrade();
        this.refreshStudentsList();
    }
});
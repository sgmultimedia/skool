<?php

namespace App\Infrastructure\Persistence\Doctrine\Repositories;

use App\Domain\Shared\Listing\PaginatedResult;
use App\Domain\Shared\Listing\Pagination;
use App\Domain\Shared\Listing\SortOptions;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

abstract class StandardRepository
{
    private EntityRepository $source;

    private EntityManagerInterface $entityManager;

    /**
     * UserRepository constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
        $this->source = $em->getRepository($this->getEntityClassName());
    }

    /**
     * @return EntityRepository
     */
    protected function getSource(): EntityRepository
    {
        return $this->source;
    }

    /**
     * @return EntityManagerInterface
     */
    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * Returns the name of the entity class to be used by the concrete repository
     *
     * @return string
     */
    protected abstract function getEntityClassName(): string;

    /**
     * Helper used to apply sort options on a query builder.
     * @param QueryBuilder $qb
     * @param SortOptions $sortOptions
     * @param array $aliases Aliases to add to the field name
     * @return QueryBuilder
     */
    protected function applySortOptions(QueryBuilder $qb, SortOptions $sortOptions, array $aliases = []): QueryBuilder
    {
        $field = trim($sortOptions->getSortedField());

        if ($field !== '') {
            $alias = $this->guessFieldAlias($qb, $field, $aliases);

            if (!is_null($alias)) {
                $field = $alias . '.' . $field;
            }

            $qb->addOrderBy($field, $sortOptions->getDirection());
        }

        return $qb;
    }

    /**
     * Guess the field alias to prefix on operations.
     *
     * @param QueryBuilder $qb
     * @param string $field
     * @param array $aliases
     * @return string|null
     */
    private function guessFieldAlias(QueryBuilder $qb, string $field, array $aliases): ?string
    {
        $alias = null;

        // Skip if an alias was already provided in the field name.
        if (strstr($field, '.') === false) {
            if (array_key_exists($field, $aliases)) {
                $alias = $aliases[$field];
            } else {
                $alias = current($qb->getRootAliases());
            }
        }

        return $alias;
    }

    /**
     * Helper used to apply pagination options on a query builder.
     * @param QueryBuilder $qb
     * @param Pagination $pagination
     * @return QueryBuilder
     */
    protected function applyPagination(QueryBuilder $qb, Pagination $pagination): QueryBuilder
    {
        $limit = $pagination->getLimit();

        if (is_int($limit)) {
            $qb->setMaxResults($limit);
        }

        $offset = $pagination->getOffset();

        if (is_int($offset)) {
            $qb->setFirstResult($offset);
        }

        return $qb;
    }

    /**
     * @param QueryBuilder $qb
     * @param Pagination $pagination
     * @return array|PaginatedResult
     */
    protected function handleResult(QueryBuilder $qb, Pagination $pagination = null)
    {
        if ($pagination instanceof Pagination) {
            $paginator = new Paginator($qb->getQuery(), false);

            $result = new PaginatedResult(
                iterator_to_array($paginator->getIterator()),
                $paginator->count()
            );
        } else {
            $result = $qb->getQuery()->execute();
        }

        return $result;
    }
}

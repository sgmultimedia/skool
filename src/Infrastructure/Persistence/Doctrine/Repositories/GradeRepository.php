<?php

namespace App\Infrastructure\Persistence\Doctrine\Repositories;

use App\Domain\Grade\Grade;
use App\Domain\Grade\GradeRepository as GradeRepositoryInterface;
use App\Domain\Shared\Listing\PaginatedResult;
use App\Domain\Shared\Listing\Pagination;
use App\Domain\Shared\Listing\SortOptions;
use App\Domain\Shared\Specification\Specification;
use App\Domain\Student\Student;

class GradeRepository extends StandardRepository implements GradeRepositoryInterface
{
    use WithSpecificationProcessorTrait;

    /**
     * Returns the name of the entity class to be used by the concrete repository
     *
     * @return string
     */
    protected function getEntityClassName(): string
    {
        return Grade::class;
    }

    public function add(Grade $grade): GradeRepositoryInterface
    {
        $this->getEntityManager()->persist($grade);

        return $this;
    }

    /**
     * @param Student|null $student
     * @return float|null
     */
    public function getAverageValue(Student $student = null): ?float
    {
        $qb = $this
            ->getEntityManager()
            ->getConnection()
            ->createQueryBuilder();

        $qb
            ->select('AVG(g.value) AS average_value')
            ->from('grade', 'g')
        ;

        if ($student instanceof Student && is_int($student->getId())) {
            $qb
                ->where('g.student_id = :student')
                ->setParameter('student', $student->getId());
        }

        return $qb->execute()->fetchColumn();
    }

    /**
     * @param Specification $specification
     * @param SortOptions $sortOptions
     * @param Pagination $pagination
     * @return Grade[]|PaginatedResult
     */
    public function findAll(Specification $specification = null, SortOptions $sortOptions = null, Pagination $pagination = null)
    {
        $qb = $this
            ->getSource()
            ->createQueryBuilder('g');

        if ($sortOptions instanceof SortOptions) {
            $this->applySortOptions($qb, $sortOptions);
        }

        if ($pagination instanceof Pagination) {
            $this->applyPagination($qb, $pagination);
        }

        if ($specification instanceof Specification) {
            $this->applySpecifications($qb, $specification);
        }

        return $this->handleResult($qb, $pagination);
    }
}

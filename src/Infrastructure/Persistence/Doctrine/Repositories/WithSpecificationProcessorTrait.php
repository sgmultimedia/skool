<?php

namespace App\Infrastructure\Persistence\Doctrine\Repositories;

use App\Domain\Shared\Specification\Specification;
use App\Infrastructure\Persistence\Doctrine\Specification\Processor;
use App\Infrastructure\Persistence\Doctrine\Specification\Processor as SpecificationProcessor;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

trait WithSpecificationProcessorTrait
{
    private Processor $specificationProcessor;

    /**
     * @param EntityManagerInterface $em
     * @param SpecificationProcessor $specificationProcessor
     */
    public function __construct(EntityManagerInterface $em, SpecificationProcessor $specificationProcessor)
    {
        parent::__construct($em);

        $this->specificationProcessor = $specificationProcessor;
    }

    /**
     * Helper used to apply specifications on the given QueryBuilder.
     *
     * @param QueryBuilder $qb
     * @param Specification $specifications
     * @return QueryBuilder
     */
    protected function applySpecifications(QueryBuilder $qb, Specification $specifications): QueryBuilder
    {
        return $this->specificationProcessor->applySpecifications($qb, $specifications);
    }
}

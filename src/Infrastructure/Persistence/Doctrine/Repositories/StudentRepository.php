<?php

namespace App\Infrastructure\Persistence\Doctrine\Repositories;

use App\Domain\Shared\Listing\PaginatedResult;
use App\Domain\Shared\Listing\Pagination;
use App\Domain\Shared\Listing\SortOptions;
use App\Domain\Shared\Specification\Specification;
use App\Domain\Student\Student;
use App\Domain\Student\StudentRepository as StudentRepositoryInterface;

class StudentRepository extends StandardRepository implements StudentRepositoryInterface
{
    use WithSpecificationProcessorTrait;

    /**
     * Returns the name of the entity class to be used by the concrete repository
     *
     * @return string
     */
    protected function getEntityClassName(): string
    {
        return Student::class;
    }

    /**
     * @param int $id
     * @return Student|null
     */
    public function find(int $id): ?Student
    {
        return $this->getSource()->find($id);
    }

    /**
     * @param array $filters
     * @return Student|null
     */
    public function findOneBy(array $filters): ?Student
    {
        return $this->getSource()->findOneBy($filters);
    }

    /**
     * @param Specification $specification
     * @param SortOptions $sortOptions
     * @param Pagination $pagination
     * @return Student[]|PaginatedResult
     */
    public function findAll(
        Specification $specification = null,
        SortOptions $sortOptions = null,
        Pagination $pagination = null
    ) {
        $qb = $this
            ->getSource()
            ->createQueryBuilder('s');

        if ($sortOptions instanceof SortOptions) {
            $this->applySortOptions($qb, $sortOptions);
        }

        if ($pagination instanceof Pagination) {
            $this->applyPagination($qb, $pagination);
        }

        if ($specification instanceof Specification) {
            $this->applySpecifications($qb, $specification);
        }

        return $this->handleResult($qb, $pagination);
    }

    /**
     * @param Student $student
     * @return StudentRepositoryInterface
     */
    public function add(Student $student): StudentRepositoryInterface
    {
        $this->getEntityManager()->persist($student);

        return $this;
    }

    /**
     * @param Student $student
     * @return StudentRepositoryInterface
     */
    public function remove(Student $student): StudentRepositoryInterface
    {
        $this->getEntityManager()->remove($student);

        return $this;
    }
}

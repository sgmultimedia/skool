<?php

namespace App\Infrastructure\Persistence\Doctrine\Specification;

use App\Domain\Shared\Specification\Specification;
use Doctrine\ORM\QueryBuilder;

/**
 * This class is used to apply very simple Specifications to a QueryBuilder.
 */
class SimpleProcessor implements Processor
{
    /**
     * Applies the given Specification on a QueryBuilder.
     *
     * @param QueryBuilder $qb
     * @param Specification $specification
     * @return QueryBuilder
     */
    public function applySpecifications(QueryBuilder $qb, Specification $specification): QueryBuilder
    {
        $rootAlias = current($qb->getRootAliases());
        $rule = $specification->getRule();

        $rule = trim(str_replace('object.', $rootAlias . '.', $rule));

        // In rare cases, a rule can be empty (from an empty composite specification by eg.)
        if ($rule !== '') {
            $qb
                ->andWhere($rule)
                ->setParameters($specification->getParameters());
        }

        return $qb;
    }
}

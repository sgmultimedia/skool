<?php

namespace App\Infrastructure\Persistence\Doctrine\Specification;

use RulerZ\Spec\Specification;
use App\Domain\Shared\Specification\Specification as DomainSpec;

class RulerZSpecificationAdapter implements Specification
{
    private DomainSpec $specification;

    public function __construct(DomainSpec $specification)
    {
        $this->specification = $specification;
    }

    /**
     * The rule representing the specification.
     */
    public function getRule(): string
    {
        $rule = str_replace('object.', '', $this->specification->getRule());

        return $rule;
    }

    /**
     * The parameters used in the specification.
     */
    public function getParameters(): array
    {
        return $this->specification->getParameters();
    }
}

<?php

namespace App\Infrastructure\Persistence\Doctrine\Specification;

use App\Domain\Shared\Specification\Specification;
use Doctrine\ORM\QueryBuilder;

interface Processor
{
    /**
     * Applies the given Specification on a QueryBuilder.
     *
     * @param QueryBuilder $qb
     * @param Specification $specification
     * @return QueryBuilder
     */
    public function applySpecifications(QueryBuilder $qb, Specification $specification): QueryBuilder;
}

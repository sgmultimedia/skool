<?php

namespace App\Infrastructure\Persistence\Doctrine\Specification;

use App\Domain\Shared\Specification\Specification;
use Doctrine\ORM\QueryBuilder;
use RulerZ\RulerZ;

class RulerZProcessorAdapter implements Processor
{
    private RulerZ $rulerZ;

    public function __construct(RulerZ $rulerZ)
    {
        $this->rulerZ = $rulerZ;
    }

    /**
     * Applies the given Specification on a QueryBuilder.
     *
     * @param QueryBuilder $qb
     * @param Specification $specification
     * @return QueryBuilder
     */
    public function applySpecifications(QueryBuilder $qb, Specification $specification): QueryBuilder
    {
        $specification = new RulerZSpecificationAdapter($specification);

        if (trim($specification->getRule()) !== '') {
            $qb = $this->rulerZ->applyFilterSpec($qb, $specification);
        }

        return $qb;
    }
}

<?php

namespace App\Infrastructure\Event;

use App\Domain\AggregateRoot;
use App\Domain\DomainEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Listener used to publish domain events at the end of an application execution (cli or http).
 */
class DomainEventPublisher
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * PublishDomainEvents constructor.
     *
     * @param EventDispatcherInterface $eventDispatcher
     * @param LoggerInterface          $logger
     */
    public function __construct(EventDispatcherInterface $eventDispatcher, LoggerInterface $logger)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->logger = $logger;
    }

    /**
     * End of an http process.
     */
    public function onKernelTerminate()
    {
        $this->execute();
    }

    /**
     * End of a cli process
     */
    public function onConsoleTerminate()
    {
        $this->execute();
    }

    /**
     * Execute the domain event publication.
     */
    protected function execute()
    {
        $roots = AggregateRoot::getRootsWithEvents();

        foreach ($roots as $root) {
            $this->publishRootEvents($root);
        }
    }

    /**
     * @param AggregateRoot $root
     */
    protected function publishRootEvents(AggregateRoot $root)
    {
        $events = $root->getRecordedEvents();
        $root->clearRecordedEvents();

        foreach ($events as $event) {
            $this->publishEventToSubscribers($event);
        }
    }

    /**
     * @param DomainEvent $event
     */
    protected function publishEventToSubscribers(DomainEvent $event)
    {
        try {
            $this->eventDispatcher->dispatch($event);

            $this->logger->info(sprintf('[domain.event] %s was published', get_class($event)));
        } catch (\Exception $e) {
            $message = sprintf(
                '%s: %s (uncaught exception) at %s line %s while publishing domain event `%s`',
                get_class($e),
                $e->getMessage(),
                $e->getFile(),
                $e->getLine(),
                get_class($event)
            );

            $this->logger->error($message, ['exception' => $e]);
        }
    }
}

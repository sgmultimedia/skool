<?php

namespace App\Infrastructure\Shared;

class Clock implements \App\Domain\Shared\Time\Clock
{
    /**
     * @return \DateTime
     * @throws \Exception
     */
    public function now(): \DateTime
    {
        return new \DateTime('now');
    }
}

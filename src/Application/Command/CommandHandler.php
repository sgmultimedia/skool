<?php

namespace App\Application\Command;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Centralizes the tool dependence.
 */
interface CommandHandler extends MessageHandlerInterface
{

}

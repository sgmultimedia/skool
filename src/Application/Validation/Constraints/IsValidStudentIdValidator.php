<?php

namespace App\Application\Validation\Constraints;

use App\Domain\Student\Student;
use App\Domain\Student\StudentRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsValidStudentIdValidator extends ConstraintValidator
{
    private StudentRepository $studentRepository;

    public function __construct(StudentRepository $studentRepository)
    {
        $this->studentRepository = $studentRepository;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     */
    public function validate($value, Constraint $constraint)
    {
        if (!ctype_digit($value)) {
            return;
        }

        $student = $this->studentRepository->find($value);

        if (!$student instanceof Student) {
            $this
                ->context
                ->buildViolation($constraint->message)
                ->setParameter('{{ id }}', $value)
                ->addViolation();
        }
    }
}

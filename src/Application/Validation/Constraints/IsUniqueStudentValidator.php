<?php

namespace App\Application\Validation\Constraints;

use App\Application\Student\RegisterStudentCommand;
use App\Domain\Student\Student;
use App\Domain\Student\StudentRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsUniqueStudentValidator extends ConstraintValidator
{
    private StudentRepository $studentRepository;

    public function __construct(StudentRepository $studentRepository)
    {
        $this->studentRepository = $studentRepository;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$value instanceof RegisterStudentCommand) {
            return;
        }

        $student = $this->studentRepository->findOneBy([
            'firstName' => $value->firstName,
            'lastName' => $value->lastName,
            'birthDate' => $value->birthDate
        ]);

        if ($student instanceof Student) {
            $this
                ->context
                ->buildViolation($constraint->message)
                ->setParameter('{{ firstName }}', $value->firstName)
                ->setParameter('{{ lastName }}', $value->lastName)
                ->setParameter('{{ birthDate }}', $value->birthDate->format('Y-m-d'))
                ->addViolation();
        }
    }
}

<?php

namespace App\Application\Validation\Constraints;

use Symfony\Component\Validator\Constraint;

class IsValidStudentId extends Constraint
{
    public $message = 'There is no Student with id {{ id }}.';

    public function getTargets()
    {
        return self::PROPERTY_CONSTRAINT;
    }
}

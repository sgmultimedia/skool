<?php

namespace App\Application\Validation\Constraints;

use Symfony\Component\Validator\Constraint;

class IsUniqueStudent extends Constraint
{
    public $message = 'There is already a registered student born on {{ birthDate }} with the name {{ firstName }} {{ lastName }}.';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}

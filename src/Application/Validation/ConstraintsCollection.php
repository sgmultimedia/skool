<?php

namespace App\Application\Validation;

use Symfony\Component\Validator\Constraint;

/**
 * Constraints container for a command validation.
 */
class ConstraintsCollection
{
    /**
     * @var Constraint[]
     */
    private array $propertiesConstraints;

    /**
     * @var Constraint[]
     */
    private array $objectConstraints;

    /**
     * ConstraintsCollection constructor.
     */
    public function __construct()
    {
        $this->propertiesConstraints = [];
        $this->objectConstraints = [];
    }

    /**
     * @param array $constraints
     * @return ConstraintsCollection
     */
    public function addObjectConstraints(array $constraints): ConstraintsCollection
    {
        foreach ($constraints as $constraint) {
            $this->addObjectConstraint($constraint);
        }

        return $this;
    }

    /**
     * @param Constraint $constraint
     * @return $this
     */
    public function addObjectConstraint(Constraint $constraint)
    {
        $this->objectConstraints[] = $constraint;

        return $this;
    }

    /**
     * @return array
     */
    public function getObjectConstraints(): array
    {
        return $this->objectConstraints;
    }

    /**
     * @param $propertyName
     * @param Constraint[] $constraints
     * @return ConstraintsCollection
     */
    public function addPropertyConstraints(string $propertyName, array $constraints): ConstraintsCollection
    {
        foreach ($constraints as $constraint) {
            $this->addPropertyConstraint($propertyName, $constraint);
        }

        return $this;
    }

    /**
     * @param $propertyName
     * @param Constraint $constraint
     * @return ConstraintsCollection
     */
    public function addPropertyConstraint(string $propertyName, Constraint $constraint): ConstraintsCollection
    {
        if (!array_key_exists($propertyName, $this->propertiesConstraints)) {
            $this->propertiesConstraints[$propertyName] = [];
        }

        $this->propertiesConstraints[$propertyName][] = $constraint;

        return $this;
    }

    /**
     * @return array
     */
    public function getPropertiesConstraints(): array
    {
        return $this->propertiesConstraints;
    }

    /**
     * @param string $propertyName
     * @return array|Constraint
     */
    public function getPropertyConstraints(string $propertyName)
    {
        $result = [];

        if (array_key_exists($propertyName, $this->propertiesConstraints)) {
            $result = $this->propertiesConstraints[$propertyName];
        }

        return $result;
    }
}

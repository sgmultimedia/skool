<?php

namespace App\Application\Validation;

use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * This trait is used to avoid the tools complexity to leak everywhere in the application layer.
 */
trait Validatable
{
    /**
     * @return ConstraintsCollection
     */
    abstract public static function getValidationConstraints(): ConstraintsCollection;

    /**
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $validationConstraints = self::getValidationConstraints();

        foreach ($validationConstraints->getObjectConstraints() as $constraint) {
            $metadata->addConstraint($constraint);
        }

        foreach ($validationConstraints->getPropertiesConstraints() as $property => $constraints) {
            $metadata->addPropertyConstraints($property, $constraints);
        }
    }
}

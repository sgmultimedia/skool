<?php

namespace App\Application\Query;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Centralizes the tool dependence.
 */
interface QueryHandler extends MessageHandlerInterface
{

}

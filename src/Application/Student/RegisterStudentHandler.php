<?php

namespace App\Application\Student;

use App\Application\Command\CommandHandler;
use App\Domain\Shared\Time\Clock;
use App\Domain\Student\Student;
use App\Domain\Student\StudentRepository;

class RegisterStudentHandler implements CommandHandler
{
    private StudentRepository $studentRepository;
    private Clock $clock;

    public function __construct(StudentRepository $studentRepository, Clock $clock)
    {
        $this->studentRepository = $studentRepository;
        $this->clock = $clock;
    }

    public function __invoke(RegisterStudentCommand $command)
    {
        $student = Student::registerStudent(
            $command->firstName,
            $command->lastName,
            $command->birthDate,
            $this->clock
        );

        $this->studentRepository->add($student);

        return $student;
    }
}

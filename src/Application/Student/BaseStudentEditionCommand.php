<?php

namespace App\Application\Student;

use App\Application\Validation\Constraints\IsUniqueStudent;
use DateTime;

use App\Application\Command\Command;
use App\Application\Validation\ConstraintsCollection;
use App\Application\Validation\Validatable;

use Symfony\Component\Validator\Constraints as Assert;

abstract class BaseStudentEditionCommand implements Command
{
    use Validatable;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var DateTime
     */
    public $birthDate;

    /**
     * @return ConstraintsCollection
     */
    public static function getValidationConstraints(): ConstraintsCollection
    {
        $namePattern = '/^[\p{L}\'][ \p{L}\'-]*[\p{L}]$/u';

        $constraints = (new ConstraintsCollection())
            ->addPropertyConstraints('firstName', [
                new Assert\Regex($namePattern),
                new Assert\NotBlank()
            ])
            ->addPropertyConstraints('lastName', [
                new Assert\Regex($namePattern),
                new Assert\NotBlank()
            ])
            ->addPropertyConstraints('birthDate', [
                new Assert\Type('\DateTimeInterface'),
                new Assert\NotBlank()
            ])
            ->addObjectConstraint(
                new IsUniqueStudent()
            )
        ;

        return $constraints;
    }
}

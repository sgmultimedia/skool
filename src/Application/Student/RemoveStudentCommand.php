<?php

namespace App\Application\Student;

use App\Application\Command\Command;
use App\Application\Validation\Constraints\IsValidStudentId;
use App\Application\Validation\ConstraintsCollection;
use App\Application\Validation\Validatable;

final class RemoveStudentCommand implements Command
{
    use Validatable;

    public int $studentId;

    /**
     * @return ConstraintsCollection
     */
    public static function getValidationConstraints(): ConstraintsCollection
    {
        $constraints = (new ConstraintsCollection())
            ->addPropertyConstraint('studentId', new IsValidStudentId());

        return $constraints;
    }
}

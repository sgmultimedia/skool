<?php

namespace App\Application\Student;

use App\Application\Command\CommandHandler;
use App\Domain\Student\Student;
use App\Domain\Student\StudentRepository;

class RemoveStudentHandler implements CommandHandler
{
    private StudentRepository $studentRepository;

    public function __construct(StudentRepository $studentRepository)
    {
        $this->studentRepository = $studentRepository;
    }

    public function __invoke(RemoveStudentCommand $command)
    {
        $student = $this->studentRepository->find($command->studentId);

        if (!$student instanceof Student) {
            throw new \InvalidArgumentException(sprintf('There is no Student with id %d', $command->studentId));
        }

        $this->studentRepository->remove($student);
    }
}

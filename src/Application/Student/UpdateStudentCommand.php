<?php

namespace App\Application\Student;

use App\Application\Validation\Constraints\IsValidStudentId;
use App\Application\Validation\ConstraintsCollection;
use App\Application\Validation\Validatable;

final class UpdateStudentCommand extends BaseStudentEditionCommand
{
    use Validatable; // Required because if it's not here, the Validator will call the parent's getValidationConstraints

    public int $studentId;

    /**
     * @return ConstraintsCollection
     */
    public static function getValidationConstraints(): ConstraintsCollection
    {
        $constraints = parent::getValidationConstraints();
        $constraints->addPropertyConstraint('studentId', new IsValidStudentId());

        return $constraints;
    }
}

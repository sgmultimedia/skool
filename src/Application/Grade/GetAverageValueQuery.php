<?php

namespace App\Application\Grade;

use App\Application\Query\Query;

class GetAverageValueQuery implements Query
{
    /**
     * @var int|null Use a studentId here to get the average value only for the corresponding Student
     */
    public ?int $studentId = null;
}

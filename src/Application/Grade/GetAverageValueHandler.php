<?php

namespace App\Application\Grade;

use App\Application\Query\QueryHandler;
use App\Domain\Grade\GradeRepository;
use App\Domain\Student\Student;
use App\Domain\Student\StudentRepository;

class GetAverageValueHandler implements QueryHandler
{
    private GradeRepository $gradeRepository;
    private StudentRepository $studentRepository;

    public function __construct(GradeRepository $gradeRepository, StudentRepository $studentRepository)
    {
        $this->gradeRepository = $gradeRepository;
        $this->studentRepository = $studentRepository;
    }

    public function __invoke(GetAverageValueQuery $query)
    {
        $student = null;

        if (is_int($query->studentId)) {
            $student = $this->getStudent($query->studentId);
        }

        return $this->gradeRepository->getAverageValue($student);
    }

    private function getStudent(int $studentId): Student
    {
        $student = $this->studentRepository->find($studentId);

        if (!$student instanceof Student) {
            throw new \InvalidArgumentException(sprintf('There is no Student with id %d', $studentId));
        }

        return $student;
    }
}

<?php

namespace App\Application\Grade;

use App\Application\Command\CommandHandler;
use App\Domain\Grade\Grade;
use App\Domain\Grade\GradeRepository;
use App\Domain\Shared\Time\Clock;
use App\Domain\Student\Student;
use App\Domain\Student\StudentRepository;

class CreateGradeHandler implements CommandHandler
{
    private GradeRepository $gradeRepository;
    private StudentRepository $studentRepository;
    private Clock $clock;

    public function __construct(GradeRepository $gradeRepository, StudentRepository $studentRepository, Clock $clock)
    {
        $this->gradeRepository = $gradeRepository;
        $this->studentRepository = $studentRepository;
        $this->clock = $clock;
    }

    public function __invoke(CreateGradeCommand $command)
    {
        $student = $this->studentRepository->find($command->studentId);

        if (!$student instanceof Student) {
            throw new \InvalidArgumentException(sprintf('There is no Student with id %d', $command->studentId));
        }

        $grade = Grade::createGradeForStudent(
            $student,
            $command->value,
            $command->subject,
            $this->clock
        );

        $this->gradeRepository->add($grade);

        return $grade;
    }
}

<?php

namespace App\Application\Grade;

use App\Application\Command\Command;
use App\Application\Validation\Constraints\IsValidStudentId;
use App\Application\Validation\ConstraintsCollection;
use App\Application\Validation\Validatable;

use Symfony\Component\Validator\Constraints as Assert;

class CreateGradeCommand implements Command
{
    use Validatable;

    /**
     * @var int
     */
    public $studentId;

    /**
     * @var int
     */
    public $value;

    /**
     * @var string
     */
    public $subject;

    /**
     * @return ConstraintsCollection
     */
    public static function getValidationConstraints(): ConstraintsCollection
    {
        $constraints = (new ConstraintsCollection())
            ->addPropertyConstraints('value', [
                new Assert\Range([
                    'min' => 0,
                    'max' => 20
                ]),
                new Assert\NotBlank()
            ])
            ->addPropertyConstraints('subject', [
                new Assert\Type('string'),
                new Assert\NotBlank()
            ])
            ->addPropertyConstraints('studentId', [
                new Assert\Type('numeric'),
                new Assert\NotBlank(),
                new IsValidStudentId()
            ])
        ;

        return $constraints;
    }
}

<?php

namespace App\Domain\Student\Specification;

use App\Domain\Shared\Specification\Specification;

class HasFirstNameLike implements Specification
{
    private string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * Returns the rule defined by the specification
     * @return string
     */
    public function getRule(): string
    {
        return 'object.firstName LIKE :firstName';
    }

    /**
     * Returns the parameters used to build the rule.
     * @return array
     */
    public function getParameters(): array
    {
        return ['firstName' => $this->name . '%'];
    }
}

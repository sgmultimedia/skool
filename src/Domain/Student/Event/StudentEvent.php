<?php

namespace App\Domain\Student\Event;

use App\Domain\DomainEvent;
use App\Domain\Student\Student;

abstract class StudentEvent extends DomainEvent
{
    private Student $student;

    public function __construct(Student $student)
    {
        $this->student = $student;
    }

    public function getAggregateRoot()
    {
        return $this->student;
    }
}

<?php

namespace App\Domain\Student\Event;

use App\Domain\Grade\Grade;
use App\Domain\Student\Student;

final class GradeWasReceived extends StudentEvent
{
    private Grade $grade;

    public final function __construct(Student $student, Grade $grade)
    {
        parent::__construct($student);

        $this->grade = $grade;
    }
}

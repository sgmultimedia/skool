<?php

namespace App\Domain\Student;

use App\Domain\Shared\Listing\PaginatedResult;
use App\Domain\Shared\Listing\Pagination;
use App\Domain\Shared\Listing\SortOptions;
use App\Domain\Shared\Specification\Specification;

interface StudentRepository
{
    public function find(int $id): ?Student;

    public function findOneBy(array $filters): ?Student;

    /**
     * @param Specification $specification
     * @param SortOptions $sortOptions
     * @param Pagination $pagination
     * @return Student[]|PaginatedResult
     */
    public function findAll(
        Specification $specification = null,
        SortOptions $sortOptions = null,
        Pagination $pagination = null
    );

    /**
     * @param Student $student
     * @return StudentRepository
     */
    public function add(Student $student): StudentRepository;

    /**
     * @param Student $student
     * @return StudentRepository
     */
    public function remove(Student $student): StudentRepository;
}

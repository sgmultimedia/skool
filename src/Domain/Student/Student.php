<?php

namespace App\Domain\Student;

use App\Domain\AggregateRoot;
use App\Domain\Grade\Grade;
use App\Domain\Shared\Time\Clock;
use App\Domain\Student\Event\GradeWasReceived;
use App\Domain\Student\Event\InformationWereUpdated;
use App\Domain\Student\Event\StudentWasRegistered;
use DateTime;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Student extends AggregateRoot
{
    private int $id;

    private string $firstName;

    private string $lastName;

    private DateTime $birthDate;

    private DateTime $registrationDate;

    /**
     * Should be a GradeCollection instead of a Doctrine Collection, but there is a known limitation on Doctrine about
     * it.
     * @var Collection
     */
    private Collection $grades;

    private function __construct()
    {
    }

    /**
     * Named constructor used to register a new student.
     * Another named constructor could be created in the futur, to import an existing student from another system for eg.
     * @param string $firstName
     * @param string $lastName
     * @param DateTime $birthDate
     * @return Student
     */
    public static function registerStudent(string $firstName, string $lastName, DateTime $birthDate, Clock $clock): Student
    {
        $self = new self;

        $self->firstName = $firstName;
        $self->lastName = $lastName;
        $self->birthDate = $birthDate;
        $self->registrationDate = $clock->now();
        $self->grades = new ArrayCollection();

        $self->recordEvent(new StudentWasRegistered($self));

        return $self;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return DateTime
     */
    public function getBirthDate(): DateTime
    {
        return $this->birthDate;
    }

    /**
     * @return Collection
     */
    public function getGrades(): Collection
    {
        return $this->grades;
    }

    /**
     * @param Grade $grade
     * @return Student
     */
    public function receiveGrade(Grade $grade): self
    {
        $this->grades->add($grade);

        $this->recordEvent(new GradeWasReceived($this, $grade));

        return $this;
    }

    /**
     * @param string $firstName
     * @param string $lastName
     * @param DateTime $birthDate
     * @return Student
     */
    public function updateInformation(string $firstName, string $lastName, DateTime $birthDate): self
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->birthDate = $birthDate;

        $this->recordEvent(new InformationWereUpdated($this));

        return $this;
    }
}

<?php

namespace App\Domain\Shared\Exception;

/**
 * Base for all exceptions concerning the domain.
 * @package App\Domain\Shared\Exception
 */
abstract class DomainException extends \Exception
{

}

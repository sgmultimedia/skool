<?php

namespace App\Domain\Shared\Time;

use App\Domain\Shared\Exception\InvalidArgumentException;
use DateTime;

final class DateRange
{
    /**
     * @var DateTime
     */
    private $start;

    /**
     * @var DateTime
     */
    private $end;

    /**
     * DateRange constructor.
     * @param DateTime $start
     * @param DateTime $end
     * @throws InvalidArgumentException
     */
    public function __construct(DateTime $start, DateTime $end)
    {
        if ($end < $start) {
            throw new InvalidArgumentException('A DateRange end date cannot be older than the start one');
        }

        $this->start = $start;
        $this->end = $end;
    }

    /**
     * @return DateTime
     */
    public function getStart(): DateTime
    {
        return $this->start;
    }

    /**
     * @return DateTime
     */
    public function getEnd(): DateTime
    {
        return $this->end;
    }

    /**
     * @param DateTime $date
     * @return bool
     */
    public function isDateInRange(DateTime $date): bool
    {
        return $this->start <= $date && $date < $this->end;
    }
}

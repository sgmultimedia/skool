<?php

namespace App\Domain\Shared\Time;

interface Clock
{
    /**
     * @return \DateTime
     */
    public function now(): \DateTime;
}

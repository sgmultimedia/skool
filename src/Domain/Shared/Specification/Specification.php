<?php

namespace App\Domain\Shared\Specification;

interface Specification
{
    /**
     * Returns the rule defined by the specification
     * @return string
     */
    public function getRule(): string;

    /**
     * Returns the parameters used to build the rule.
     * @return array
     */
    public function getParameters(): array;
}

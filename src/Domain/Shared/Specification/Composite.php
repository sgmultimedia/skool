<?php

namespace App\Domain\Shared\Specification;

abstract class Composite implements Specification
{
    /**
     * @var Specification[]
     */
    private array $specifications = [];

    /**
     * @var string AND / OR
     */
    private string $operator;

    /**
     * Composite constructor.
     * @param string $operator
     * @param array $specifications
     */
    protected function __construct(string $operator, array $specifications)
    {
        $this->operator = $operator;

        foreach ($specifications as $specification) {
            $this->add($specification);
        }
    }

    /**
     * @return array
     */
    protected function getSpecifications(): array
    {
        return $this->specifications;
    }

    /**
     * @return string[]
     */
    protected function extractRules(): array
    {
        return array_map(
            function (Specification $specification) {
                return sprintf('(%s)', $specification->getRule());
            },
            $this->specifications
        );
    }

    /**
     * @param array $specifications
     * @return self
     */
    public final static function fromArray(array $specifications): self
    {
        $self = new static();

        foreach ($specifications as $specification) {
            $self->add($specification);
        }

        return $self;
    }

    public final function add(Specification $specification): self
    {
        $this->specifications[] = $specification;

        return $this;
    }

    /**
     * Returns the parameters used to build the rule.
     * @return array
     */
    public function getParameters(): array
    {
        $parameters = [];

        foreach ($this->specifications as $specification) {
            $parameters = array_merge($parameters, $specification->getParameters());
        }

        return $parameters;
    }

    /**
     * Returns the rule defined by the specification
     * @return string
     */
    public function getRule(): string
    {
        $rule = '';
        $rules = $this->extractRules();

        if (count($rules) > 0) {
            $rule = '(' . implode(' ' . $this->operator . ' ', $rules) . ')';
        }

        return $rule;
    }
}

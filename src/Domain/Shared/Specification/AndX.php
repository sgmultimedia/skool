<?php

namespace App\Domain\Shared\Specification;

class AndX extends Composite implements Specification
{
    public function __construct(Specification ...$specifications)
    {
        parent::__construct('AND', $specifications);
    }
}

<?php

namespace App\Domain\Shared\Specification;

class OrX extends Composite implements Specification
{
    public function __construct(Specification ...$specifications)
    {
        parent::__construct('OR', $specifications);
    }
}

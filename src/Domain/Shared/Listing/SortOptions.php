<?php

namespace App\Domain\Shared\Listing;

use App\Domain\Shared\Exception\InvalidArgumentException;

class SortOptions
{
    const SORT_ASC = 'ASC';
    const SORT_DESC = 'DESC';

    private string $field;
    private string $direction = self::SORT_ASC;

    public function __construct(string $field = null, string $direction = null)
    {
        $this->field = $field;

        if (!is_null($direction)) {
            $this->setSortDirection($direction);
        }
    }

    public function sortBy(string $field): self
    {
        $this->field = $field;

        return $this;
    }

    public function setSortDirection(string $direction): self
    {
        $direction = strtoupper($direction);

        if (!in_array($direction, [self::SORT_ASC, self::SORT_DESC])) {
            throw new InvalidArgumentException(sprintf('%s is not a valid sort direction', $direction));
        }

        $this->direction = $direction;

        return $this;
    }

    public function asc(): self
    {
        $this->direction = self::SORT_ASC;

        return $this;
    }

    public function desc(): self
    {
        $this->direction = self::SORT_DESC;

        return $this;
    }

    public function getSortedField(): ?string
    {
        return $this->field;
    }

    public function getDirection(): string
    {
        return $this->direction;
    }
}

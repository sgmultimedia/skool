<?php

namespace App\Domain\Shared\Listing;

class PaginatedResult
{
    private array $slice;
    private int $count;
    private int $totalCount;

    public function __construct(array $slice, int $totalCount)
    {
        $this->slice = $slice;
        $this->count = count($slice);
        $this->totalCount = $totalCount;
    }

    /**
     * @return array
     */
    public function getSlice(): array
    {
        return $this->slice;
    }

    /**
     * @return int|void
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
        return $this->totalCount;
    }
}

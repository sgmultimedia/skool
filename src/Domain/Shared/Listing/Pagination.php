<?php

namespace App\Domain\Shared\Listing;

class Pagination
{
    private int $limit;
    private int $offset;

    public function __construct(int $limit = null, int $offset = null)
    {
        $this->limit = $limit;
        $this->offset = $offset;
    }

    public function getLimit(): ?int
    {
        return $this->limit;
    }

    public function getOffset(): ?int
    {
        return $this->offset;
    }
}

<?php

namespace App\Domain;

use Symfony\Contracts\EventDispatcher\Event;

abstract class DomainEvent extends Event
{
    /**
     * @return AggregateRoot
     */
    public abstract function getAggregateRoot();
}

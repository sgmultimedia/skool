<?php

namespace App\Domain;

/**
 * Base class for all aggregates.
 */
abstract class AggregateRoot
{
    /**
     * @var DomainEvent[]
     */
    private $unpublishedEvents = [];

    /**
     * @var AggregateRoot[]
     */
    private static $rootsWithEvents = [];

    /**
     * @return array
     */
    public static function getRootsWithEvents(): array
    {
        return self::$rootsWithEvents;
    }

    /**
     * @param DomainEvent $event
     * @return $this
     */
    protected function recordEvent(DomainEvent $event)
    {
        $this->unpublishedEvents[] = $event;
        self::$rootsWithEvents[] = $event->getAggregateRoot();

        return $this;
    }

    /**
     * @return DomainEvent[]
     */
    public function getRecordedEvents()
    {
        return $this->unpublishedEvents;
    }

    /**
     * @return $this
     */
    public function clearRecordedEvents()
    {
        $this->unpublishedEvents = [];

        return $this;
    }
}

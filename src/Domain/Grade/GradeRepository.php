<?php

namespace App\Domain\Grade;

use App\Domain\Shared\Listing\PaginatedResult;
use App\Domain\Shared\Listing\Pagination;
use App\Domain\Shared\Listing\SortOptions;
use App\Domain\Shared\Specification\Specification;
use App\Domain\Student\Student;

interface GradeRepository
{
    public function add(Grade $grade): GradeRepository;

    public function getAverageValue(Student $student = null): ?float;

    /**
     * @param Specification $specification
     * @param SortOptions $sortOptions
     * @param Pagination $pagination
     * @return Grade[]|PaginatedResult
     */
    public function findAll(
        Specification $specification = null,
        SortOptions $sortOptions = null,
        Pagination $pagination = null
    );
}

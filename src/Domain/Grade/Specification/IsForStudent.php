<?php

namespace App\Domain\Grade\Specification;

use App\Domain\Shared\Specification\Specification;
use App\Domain\Student\Student;

class IsForStudent implements Specification
{
    private Student $student;

    public function __construct(Student $student)
    {
        $this->student = $student;
    }

    /**
     * Returns the rule defined by the specification
     * @return string
     */
    public function getRule(): string
    {
        return 'object.student = :gradeHasStudent';
    }

    /**
     * Returns the parameters used to build the rule.
     * @return array
     */
    public function getParameters(): array
    {
        return ['gradeHasStudent' => $this->student];
    }
}

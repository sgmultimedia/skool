<?php

namespace App\Domain\Grade;

use App\Domain\AggregateRoot;
use App\Domain\Shared\Time\Clock;
use App\Domain\Student\Student;
use DateTime;

class Grade extends AggregateRoot
{
    private int $id;

    private Student $student;

    /**
     * @var int A value between 0 and 20
     */
    private int $value;

    private string $subject;

    private DateTime $creationDate;

    private function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getStudent(): Student
    {
        return $this->student;
    }

    public static function createGradeForStudent(Student $student, int $value, string $subject, Clock $clock): Grade
    {
        $grade = new self;

        $grade->student = $student;
        $grade->value = $value;
        $grade->subject = $subject;
        $grade->creationDate = $clock->now();

        $student->receiveGrade($grade);

        return $grade;
    }
}

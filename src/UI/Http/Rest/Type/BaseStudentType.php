<?php

namespace App\UI\Http\Rest\Type;

use App\Application\Student\RegisterStudentCommand;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class BaseStudentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'first_name',
                TextType::class,
                [
                    'property_path' => 'firstName'
                ]
            )
            ->add(
                'last_name',
                TextType::class,
                [
                    'property_path' => 'lastName'
                ]
            )
            ->add(
                'birth_date',
                DateType::class,
                [
                    'property_path' => 'birthDate',
                    'widget' => 'single_text'
                ]
            )
        ;
    }
}

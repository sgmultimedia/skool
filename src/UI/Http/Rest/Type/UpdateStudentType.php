<?php

namespace App\UI\Http\Rest\Type;

use App\Application\Student\UpdateStudentCommand;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UpdateStudentType extends BaseStudentType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UpdateStudentCommand::class,
        ]);
    }
}

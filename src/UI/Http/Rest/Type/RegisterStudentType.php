<?php

namespace App\UI\Http\Rest\Type;

use App\Application\Student\RegisterStudentCommand;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegisterStudentType extends BaseStudentType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RegisterStudentCommand::class,
        ]);
    }
}

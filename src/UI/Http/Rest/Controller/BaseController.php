<?php

namespace App\UI\Http\Rest\Controller;

use App\Domain\Shared\Listing\PaginatedResult;
use App\Domain\Shared\Listing\Pagination;
use App\Domain\Shared\Listing\SortOptions;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Hateoas\Representation\CollectionRepresentation;
use Hateoas\Representation\PaginatedRepresentation;

abstract class BaseController extends AbstractFOSRestController
{

    /**
     * Helper used to create a Pagination object from the parameters given in the ParamFetcher
     *
     * @param ParamFetcherInterface $paramFetcher
     * @param int $default
     * @param int $max
     * @return Pagination
     */
    protected function applyPagination(ParamFetcherInterface $paramFetcher, int $default = 20, int $max = 100): Pagination
    {
        $limit = (int) $paramFetcher->get('limit');

        if ($limit < 1 || $limit > $max) {
            $limit = $default;
        }

        $page = $paramFetcher->get('page');

        if ($page < 1) {
            $page = 1;
        }

        return new Pagination($limit, ($page - 1) * $limit);
    }

    /**
     * Helper used to create a SortOptions object from the parameters givent in the ParamFetcher
     *
     * @param ParamFetcherInterface $paramFetcher
     * @param string|null $defaultField
     * @return SortOptions|null
     * @throws \App\Domain\Shared\Exception\InvalidArgumentException
     */
    protected function applySortOptions(
        ParamFetcherInterface $paramFetcher,
        string $defaultField = null
    ): ?SortOptions
    {
        $result = null;
        $field = trim($paramFetcher->get('sort'));

        if (empty($field) && !empty($defaultField)) {
            $field = $defaultField;
        }

        if (!empty($field)) {
            $direction = 'ASC';

            if (substr($field, 0, 1) === '-') {
                $direction = 'DESC';

                $field = substr($field, 1);
            }

            $result = new SortOptions();
            $result
                ->sortBy($field)
                ->setSortDirection($direction);
        }

        return $result;
    }

    /**
     * Helper to create a paginated representation
     *
     * @param PaginatedResult $result
     * @param Pagination $pagination
     * @param string $routeName
     * @param array $routeParameters
     * @return PaginatedRepresentation
     */
    protected function createPaginatedRepresentation(
        PaginatedResult $result,
        Pagination $pagination,
        string $routeName = '',
        array $routeParameters = []
    ): PaginatedRepresentation
    {
        $paginatedRepresentation = new PaginatedRepresentation(
            new CollectionRepresentation($result->getSlice()),
            $routeName,
            $routeParameters,
            floor($pagination->getOffset() / $pagination->getLimit()) + 1, // Page number
            $pagination->getLimit(),
            ceil($result->getTotalCount() / $pagination->getLimit()) // Total pages
        );

        return $paginatedRepresentation;
    }
}

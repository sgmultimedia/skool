<?php

namespace App\UI\Http\Rest\Controller;

use App\Application\Student\RegisterStudentCommand;
use App\Application\Student\RemoveStudentCommand;
use App\Application\Student\UpdateStudentCommand;
use App\Domain\Shared\Specification\AndX;
use App\Domain\Shared\Specification\Specification;
use App\Domain\Student\Specification\HasFirstNameLike;
use App\Domain\Student\Specification\HasLastNameLike;
use App\Domain\Student\Student;
use App\Domain\Student\StudentRepository;

use App\UI\Http\Rest\Type\RegisterStudentType;
use App\UI\Http\Rest\Type\UpdateStudentType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;

use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

class StudentController extends BaseController
{
    private StudentRepository $studentRepository;

    public function __construct(StudentRepository $studentRepository)
    {
        $this->studentRepository = $studentRepository;
    }

    /**
     * Returns the Student with the given id
     *
     * @SWG\Response(
     *     response=200,
     *     description="Student was found",
     *     @Model(type=Student::class)
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Student was not found"
     * )
     *
     * @SWG\Tag(name="Students")
     *
     * @param int $id
     * @return \FOS\RestBundle\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function getStudentAction(int $id)
    {
        $student = $this->studentRepository->find($id);

        if (!$student instanceof Student) {
            return $this->view(sprintf('There is no Student with id %d', $id), Response::HTTP_NOT_FOUND);
        }

        $view = $this->view($student);

        return $this->handleView($view);
    }

    /**
     * List all the students matching given filters
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the students",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=Student::class))
     *     )
     * )
     *
     * @SWG\Tag(name="Students")
     *
     * @Rest\QueryParam(name="first_name", description="Search in the student's first name")
     * @Rest\QueryParam(name="last_name", description="Search in the student's last name")
     * @Rest\QueryParam(name="sort", description="Sort the result on this field. Precede with '-' for DESC sorting.")
     * @Rest\QueryParam(name="limit", requirements="\d+", strict=true, nullable=true, description="Item count limit", default=20)
     * @Rest\QueryParam(name="page", requirements="\d+", strict=true, nullable=true, description="Page number", default=1)
     *
     * @param ParamFetcherInterface $paramFetcher
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Domain\Shared\Exception\InvalidArgumentException
     */
    public function getStudentsAction(ParamFetcherInterface $paramFetcher)
    {
        $students = $this->studentRepository->findAll(
            $this->applySpecifications($paramFetcher),
            $this->applySortOptions($paramFetcher),
            $pagination = $this->applyPagination($paramFetcher)
        );

        $paginatedRepresentation = $this->createPaginatedRepresentation(
            $students,
            $pagination,
            'get_students',
            $paramFetcher->all()
        );

        $view = $this->view($paginatedRepresentation);

        return $this->handleView($view);
    }

    /**
     * Helper used to create a Specification from the filters given in the ParamFetcher.
     *
     * @param ParamFetcherInterface $paramFetcher
     * @return Specification
     */
    private function applySpecifications(ParamFetcherInterface $paramFetcher): Specification
    {
        $specification = new AndX();

        if (($firstName = trim($paramFetcher->get('first_name'))) && $firstName !== '') {
            $specification->add(new HasFirstNameLike($firstName));
        }

        if (($lastName = trim($paramFetcher->get('last_name'))) && $lastName !== '') {
            $specification->add(new HasLastNameLike($lastName));
        }

        return $specification;
    }

    /**
     * Registers a new Student
     *
     * @SWG\Response(
     *     response=201,
     *     description="Student was registered",
     *     @Model(type=Student::class)
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Input contains errors"
     * )
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     @Model(type=RegisterStudentType::class)
     * )
     *
     * @SWG\Tag(name="Students")
     *
     * @param MessageBusInterface $bus
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postStudentAction(MessageBusInterface $bus, Request $request)
    {
        $command = new RegisterStudentCommand();

        $form = $this->createForm(RegisterStudentType::class, $command);

        $form->submit(json_decode($request->getContent(), true));

        if ($form->isValid()) {
            $resultEnvelope = $bus->dispatch($command);

            /** @var Student $student */
            $student = $resultEnvelope->last(HandledStamp::class)->getResult();

            $view = $this->view($student, Response::HTTP_CREATED);

            return $this->handleView($view);
        }

        $view = $this->view(['form' => $form], Response::HTTP_UNPROCESSABLE_ENTITY);

        return $this->handleView($view);
    }

    /**
     * Updates an existing Student's information.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Student was updated"
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Input contains errors"
     * )
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     @Model(type=RegisterStudentType::class)
     * )
     *
     * @SWG\Tag(name="Students")
     *
     * @param int $id
     * @param MessageBusInterface $bus
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function putStudentAction(int $id, MessageBusInterface $bus, Request $request)
    {
        $command = new UpdateStudentCommand();
        $command->studentId = $id;

        $form = $this->createForm(UpdateStudentType::class, $command);

        $form->submit(json_decode($request->getContent(), true));

        if ($form->isValid()) {
            $bus->dispatch($command);

            $view = $this->view(null, Response::HTTP_NO_CONTENT);

            return $this->handleView($view);
        }

        $view = $this->view(['form' => $form], Response::HTTP_UNPROCESSABLE_ENTITY);

        return $this->handleView($view);
    }

    /**
     * Deletes a student from the database.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Student was removed"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Bad request"
     * )
     *
     * @SWG\Tag(name="Students")
     *
     * @param int $id
     * @param MessageBusInterface $bus
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteStudentAction(int $id, MessageBusInterface $bus)
    {
        $command = new RemoveStudentCommand();
        $command->studentId = $id;

        try {
            $bus->dispatch($command);
        } catch (HandlerFailedException $e) {
            $view = $this->view($e->getMessage(), Response::HTTP_BAD_REQUEST);

            return $this->handleView($view);
        }


        $view = $this->view(null, Response::HTTP_NO_CONTENT);

        return $this->handleView($view);
    }
}

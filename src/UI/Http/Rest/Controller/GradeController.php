<?php

namespace App\UI\Http\Rest\Controller;

use App\Application\Grade\CreateGradeCommand;
use App\Application\Grade\GetAverageValueHandler;
use App\Application\Grade\GetAverageValueQuery;
use App\Domain\Grade\Grade;
use App\Domain\Grade\GradeRepository;
use App\Domain\Grade\Specification\IsForStudent;
use App\Domain\Shared\Listing\SortOptions;
use App\Domain\Shared\Specification\AndX;
use App\Domain\Shared\Specification\Specification;
use App\Domain\Student\Student;
use App\Domain\Student\StudentRepository;

use App\UI\Http\Rest\Type\CreateGradeType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;

use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

class GradeController extends BaseController
{
    private GradeRepository $gradeRepository;
    private StudentRepository $studentRepository;

    public function __construct(GradeRepository $gradeRepository, StudentRepository $studentRepository)
    {
        $this->gradeRepository = $gradeRepository;
        $this->studentRepository = $studentRepository;
    }

    /**
     * Returns the Grade with the given id
     *
     * @SWG\Response(
     *     response=200,
     *     description="Grade was found",
     *     @Model(type=Grade::class)
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Grade was not found"
     * )
     *
     * @SWG\Tag(name="Grades")
     *
     * @param int $id
     * @return \FOS\RestBundle\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function getGradeAction(int $id)
    {
        $grade = $this->gradeRepository->find($id);

        if (!$grade instanceof Grade) {
            return $this->view(sprintf('There is no Grade with id %d', $id), 404);
        }

        $view = $this->view($grade);

        return $this->handleView($view);
    }

    /**
     * List all the grades matching the given filters
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the grades",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=Grade::class))
     *     )
     * )
     *
     * @SWG\Tag(name="Grades")
     *
     * @Rest\QueryParam(name="student_id", requirements="\d+", strict=true, nullable=true, description="Only grades of this student")
     * @Rest\QueryParam(name="sort", description="Sort the result on this field. Precede with '-' for DESC sorting.")
     * @Rest\QueryParam(name="limit", requirements="\d+", strict=true, nullable=true, default=20, description="Item count limit")
     * @Rest\QueryParam(name="page", requirements="\d+", strict=true, nullable=true, description="Page number", default=1)
     *
     * @param ParamFetcherInterface $paramFetcher
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Domain\Shared\Exception\InvalidArgumentException
     */
    public function getGradesAction(ParamFetcherInterface $paramFetcher)
    {
        $grades = $this->gradeRepository->findAll(
            $this->applySpecifications($paramFetcher),
            $this->applySortOptions($paramFetcher),
            $pagination = $this->applyPagination($paramFetcher)
        );

        $paginatedRepresentation = $this->createPaginatedRepresentation(
            $grades,
            $pagination,
            'get_grades',
            $paramFetcher->all()
        );

        $view = $this->view($paginatedRepresentation);

        return $this->handleView($view);
    }

    /**
     * List all the grades of the given Student
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the grades of the given student",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=Grade::class))
     *     )
     * )
     *
     * @SWG\Tag(name="Grades")
     *
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getStudentGradesAction(int $id)
    {
        $student = $this->studentRepository->find($id);

        if (!$student instanceof Student) {
            return $this->handleView(
                $this->view(sprintf('There is no Student with id %d', $id), 404)
            );
        }

        $grades = $this->gradeRepository->findAll(
            new IsForStudent($student),
            new SortOptions('creationDate', 'DESC')
        );

        $view = $this->view($grades);

        return $this->handleView($view);
    }

    /**
     * Helper used to create a Specification from the filters given in the ParamFetcher.
     *
     * @param ParamFetcherInterface $paramFetcher
     * @return Specification
     */
    private function applySpecifications(ParamFetcherInterface $paramFetcher): Specification
    {
        $specification = new AndX();

        if (
            ctype_digit($studentId = $paramFetcher->get('student_id')) &&
            ($student = $this->studentRepository->find($studentId)) instanceof Student
        ) {
            $specification->add(new IsForStudent($student));
        }

        return $specification;
    }

    /**
     * Creates a new Grade
     *
     * @SWG\Response(
     *     response=201,
     *     description="Grade was created",
     *     @Model(type=Grade::class)
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Input contains errors"
     * )
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     @Model(type=CreateGradeType::class)
     * )
     *
     * @SWG\Tag(name="Grades")
     *
     * @param MessageBusInterface $bus
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postGradeAction(int $id, MessageBusInterface $bus, Request $request)
    {
        $command = new CreateGradeCommand();
        $command->studentId = $id;

        $form = $this->createForm(CreateGradeType::class, $command);

        $form->submit(json_decode($request->getContent(), true));

        if ($form->isValid()) {
            $resultEnvelope = $bus->dispatch($command);

            /** @var Grade $grade */
            $grade = $resultEnvelope->last(HandledStamp::class)->getResult();

            $view = $this->view($grade, Response::HTTP_CREATED);

            return $this->handleView($view);
        }

        $view = $this->view(['form' => $form], Response::HTTP_UNPROCESSABLE_ENTITY);

        return $this->handleView($view);
    }

    /**
     * Returns the average Grade value
     *
     * @SWG\Response(
     *     response=200,
     *     description="Average value"
     * )
     *
     * @SWG\Tag(name="Grades")
     *
     * @param int $id
     * @return \FOS\RestBundle\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function getAverageValueAction(GetAverageValueHandler $handler)
    {
        $averageValue = $handler(new GetAverageValueQuery());

        $view = $this->view($averageValue);

        return $this->handleView($view);
    }

    /**
     * Returns the average Grade value for the given student
     *
     * @SWG\Response(
     *     response=200,
     *     description="Average value"
     * )
     *
     * @SWG\Tag(name="Grades")
     *
     * @param int $id
     * @return \FOS\RestBundle\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function getStudentAverageGradeAction(int $id, GetAverageValueHandler $handler)
    {
        $query = new GetAverageValueQuery();
        $query->studentId = $id;

        try {
            $averageValue = $handler($query);

            $view = $this->view($averageValue);
        } catch (\Throwable $e) {
            $view = $this->view($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }
}

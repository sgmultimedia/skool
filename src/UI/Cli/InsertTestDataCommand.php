<?php

namespace App\UI\Cli;

use App\Domain\Student\Student;
use Doctrine\DBAL\Connection;
use Faker\Generator as FakerGenerator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InsertTestDataCommand extends Command
{
    private FakerGenerator $faker;

    private Connection $connection;

    public function __construct(Connection $connection, string $name = null)
    {
        parent::__construct($name);

        $this->faker = \Faker\Factory::create('fr_FR');
        $this->connection = $connection;
    }

    /**
     * Command configuration
     */
    protected function configure()
    {
        $this
            ->setName('app:db:insert_test_data')
            ->setDescription('Command used to insert test data into database.')
            ->addArgument(
                'limit',
                InputArgument::OPTIONAL,
                'Maximum number of students to insert',
                30
            )
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $limit = (int) $input->getArgument('limit');

        for ($i = 0; $i < $limit; $i++) {
            $student = $this->createNewStudent();

            $output->writeln(
                sprintf('Student %s %s was created.', strtoupper($student['last_name']), $student['first_name'])
            );
        }

        return 0;
    }

    /**
     * In a perfect world, this code should not be here but in a service.
     * But it's a one shot command, only for demo purpose so ... it's ok :)
     *
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function createNewStudent(): array
    {
        $studentData = [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'birth_date' => $this->faker->dateTimeBetween('-11 years', '-9years')->format('Y-m-d'),
            'registration_date' => (new \DateTime())->format('Y-m-d H:i:s')
        ];

        $this
            ->connection
            ->insert('student', $studentData);

        $studentId = $this->connection->lastInsertId();

        $gradesCount = rand(0, 20);
        $subjects = ['maths', 'français', 'ping pong', 'géographie', 'sieste'];

        for ($i = 0; $i < $gradesCount; $i++) {
            $this
                ->connection
                ->insert(
                    'grade',
                    [
                        'student_id' => $studentId,
                        'subject' => $subjects[array_rand($subjects)],
                        'value' => rand(0, 20),
                        'creation_date' => (new \DateTime())->format('Y-m-d H:i:s')
                    ]
                );
        }

        return $studentData;
    }
}
